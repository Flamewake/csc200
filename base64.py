class Base64Converter:
  def __init__(self):
      """
      Create a string containing the Base64 digits for encoding and a
      dictionary containing the numerical value of each digit character
      for decoding.
      """
      self.digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
      self.digits += 'abcdefghijklmnopqrstuvwxyz0123456789+/'
      self.digpos = {}
      for pos, dig in enumerate(self.digits):
          self.digpos[dig] = pos

  def encode3bytes(self, bytes3):
      digits = self.digits
      if not isinstance(bytes3, bytes) or len(bytes3) < 1 or len(bytes3) > 3:
          raise ValueError('Input should be 1 to 3 bytes')

      # get first byte and first base64 character (6 bits) from it
      b1 = bytes3[0]
      index1 = b1 >> 2

      # Handle one byte case
      if len(bytes3) == 1:
          # 2nd base64 character is 2 lower order bits left shifted by 4
          index2 = (b1 & 3) << 4
          return f'{digits[index1]}{digits[index2]}=='

      b2 = bytes3[1]
      # join last 2 bits of b1 shifted left 4 with first 4 bits of b2
      index2 = (b1 & 3) << 4 | b2 >> 4

      # Handle two byte case
      if len(bytes3) == 2:
          # 3rd base64 character is lower order 4 bits of 2nd byte left shited 2
          index3 = (b2 & 15) << 2
          return f'{digits[index1]}{digits[index2]}{digits[index3]}='

      b3 = bytes3[2]
      # join last 4 bits of b2 shifted left 2 with first 2 bits of b3
      index3 = (b2 & 15) << 2 | (b3 & 192) >> 6
      # get last 6 bits of b3
      index4 = b3 & 63
      return f'{digits[index1]}{digits[index2]}{digits[index3]}{digits[index4]}'


  def decode4chars(self, s):
      digits = self.digits
      if not isinstance(s, str) or len(s) != 4 or \
              not all([ch in digits for ch in s[:2]]) or \
              not all([ch in digits + '=' for ch in s[2:]]):
          raise ValueError(f'{s} is not a base64 encoded string')

      # Compute first byte
      int1 = digits.index(s[0])
      int2 = digits.index(s[1])
      b1 = (int1 << 2) | ((int2 & 48) >> 4)

      # Handle single byte return case
      if s[2:] == '==':
          return bytes([b1])

      int3 = digits.index(s[2])
      b2 = (int2 & 15) << 4 | int3 >> 2

      # Handle 2 byte return case
      if s[3:] == '=':
          return bytes([b1, b2])

      int4 = digits.index(s[3])
      b3 = (int3 & 3) << 6 | int4

      return bytes([b1, b2, b3])


  def encode(self, f):
      """
      Read the contents of a binary file and return a base64 encoded string.
      """
      s = ''
      data = f.read(3)

      while data:
          s += self.encode3bytes(data)
          data = f.read(3)

      return s


  def decode(self, f):
      """
      Read the contents of a base64 encoded text file and convert it bytes.
      """
      bs = ''
      for pos, dig in enumerate(self.digits):
          self.digpos[dig] = pos
      bs = bytes()
      data = f.read(4)

      while data:
          bs += self.decode4chars(data)
          data = f.read(4)

      return bs


  if __name__ == '__main__':
      import doctest
      doctest.testmod()
