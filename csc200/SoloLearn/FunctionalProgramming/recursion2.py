def is_even(x):
  if x == 0: # She loves me!
    return True
  else:
    return is_odd(x-1) # Pulling off a petal

def is_odd(x):
  return not is_even(x) # She not loves me!

# Imagine you've got a flower, with x being the number of petals on the flower
# You begin to pull petals off, until there are none left
# As you pull the petals off, you alternate between saying "She loves me!" and "She loves me not!"
# The script above will slowly count down from x to 0, with is_even or is_odd alternating -1s.
# Starting small, say x is equal to 1 and we are using the function is_even.
# 1 is not 0, so True is not returned. 
# Then, 1 is subtracted from x, leaving us with 0.
# is_odd then uses is_even to check if x is 0.
# x is 0 so is_even starts to return True, but is_odd has a trick up its sleeve
# See the not in front of is_even inside is_odd?
# That reverses the True, making it False, which is correct, 1 is not even.



# Testing statements
print(is_odd(17))
print(is_even(23))
