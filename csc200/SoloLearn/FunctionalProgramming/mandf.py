def sub_two(x):
    return x - 2

nums = [3, 6, 12, 24, 48, 96]
result = list(map(sub_two, nums))
print(result)
