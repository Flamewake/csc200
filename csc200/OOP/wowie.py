class Dog(object):
    """docstring for Dog"""
    def __init__(self, name, age):
        super(Dog, self).__init__()
        self.name = name
        self.age = age
Fido = Dog("Fido", 5)
print(Fido.name)
#^ That's how you print stuff from a class I guess.       
#Attributes are data in a class
#Functions in a class are called methods.
#Dog class attributes name and age, with init method, make code to show an istance of a dog that is named Fido and 5 years old
